package com.company;

import com.company.model.Block;
import com.company.model.Blockchain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class Main {

    private static final int DIFFICULTY = 5;
    private static final int BLOCKCHAIN_MAX_SIZE = 4;

    public static void main(String[] args) {

        Blockchain blockchain = new Blockchain(DIFFICULTY, BLOCKCHAIN_MAX_SIZE);

        List<MinerBlock> minerBlockList = Arrays.asList(
                new MinerBlock("First Miner", blockchain),
                new MinerBlock("Second Miner", blockchain)
        );

        var executorService = Executors.newFixedThreadPool(minerBlockList.size());

        List<Future<?>> minerBlockListFuture = minerBlockList.stream().map(
                minerBlock -> executorService.submit(minerBlock::run)
        ).collect(Collectors.toList());


        minerBlockListFuture.forEach(minerBlockFuture -> {
            try {
                minerBlockFuture.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
        });

        executorService.shutdown();

        blockchain.getBlockchain().forEach(System.out::println);
        System.out.println("Check BlockChain " + blockchain.isChainValid());

    }

}
