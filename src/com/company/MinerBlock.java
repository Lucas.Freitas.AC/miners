package com.company;

import com.company.model.Block;
import com.company.model.Blockchain;

public class MinerBlock {

    private String minerName;

    private Blockchain blockchain;

    public MinerBlock(String minerName, Blockchain blockchain) {
        this.minerName = minerName;
        this.blockchain = blockchain;
    }

    public void run() {

        while (blockchain.getBlockchain().size() < blockchain.getMaxSizeBlockChain()) {
            if (blockchain.getBlockchain().isEmpty()) {
                var blockStartCandidate = new Block("Start Block " + minerName, "0");
                var result = blockStartCandidate.mineBlock(blockchain.getDifficulty(), minerName);
                blockchain.addBlockToBlockChain(result);
            } else {
                var newBlocksCandidate = new Block("Other Block " + minerName + blockchain.getBlockchain().size(), blockchain.getBlockchain().get(blockchain.getBlockchain().size() - 1).getHash());
                var result = newBlocksCandidate.mineBlock(blockchain.getDifficulty(), minerName);
                blockchain.addBlockToBlockChain(result);
            }

        }

    }

}
